$( document ).ready(function() {

var json = {"A":[{
  "audio": "sugar_plum.mp3",
  "image": "sugar_plum.jpg",
  "tagline": "sugar plum",
  "playing": false
}],
"B":[{
  "audio": "christmas_ringtone.mp3",
  "image": "christmas_ringtone.jpg",
  "tagline": "christmas ringtone",
  "playing": false
}],
"C":[{
  "audio": "jingle_bell_rocks.mp3",
  "image": "jingle_bell_rocks.jpg",
  "tagline": "jingle bell rocks",
  "playing": false
},]};
function generateRandom(max) {
    var num = Math.floor(Math.random() * max);
    return num;
}

function addIMG(key, path) {
  // $("#wrapper").append().hide().fadeIn(400);
  $("<img id='img-"+key+"' src='" + path + "'>").appendTo("#wrapper").hide().fadeIn(400);
  var left = generateRandom(window.innerWidth-200);
  var top = generateRandom(window.innerHeight-200);
  $("#wrapper > img").last().css({"position":"absolute","top": top + "px", "left": left + "px"});
}

  // detect keypress and trigger audio
  $(document).keyup(function(e) {
    var keycode = e.keyCode ||e.which;
    var key = String.fromCharCode(keycode);

    // var name = keycode + ' ' + key + ' is pressed.';
    // $('#keyname').text(name);

    var obj = json[key];

    if(typeof obj != "undefined" && !obj[0].playing){
        // console.log(obj[0].image);
        var img = obj[0].image;
        var audio = obj[0].audio;

        addIMG(keycode, "img/"+img);
        // $('#picture').fadeOut(400, function() {
        //     $("#picture").attr('src',"img/"+img);
        // }).fadeIn(400);

        var snd = new Audio("sound/"+audio); // buffers automatically when created

        // reset flag when stoped
        snd.addEventListener("ended", function(){
             obj[0].playing = false;
             $("#img-"+keycode).fadeOut(400, function(){
               $("#img-"+keycode).remove();
            });
        });

        snd.play();
        // set flag when playing so avoid duplication
        obj[0].playing = true;
    }

  });
});
